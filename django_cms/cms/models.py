from django.db import models
from django.contrib.auth.models import AbstractUser
from sorl.thumbnail import ImageField
from django.utils.translation import ugettext as _
from djrichtextfield.models import RichTextField
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from datetime import datetime


# Extend the Django base User so we have a place for future logic. Easier than refactoring later.
class User(AbstractUser):
    email = models.EmailField(_('email address'), unique=True)

    def __str__(self):
        return self.get_full_name() or self.username

    def get_admin_groups(self):
        admin_memberships = self.membership_set.filter(is_admin=True)
        admin_groups = [x.group.id for x in admin_memberships]
        admin_groups = PublishGroup.objects.filter(id__in=admin_groups)
        return admin_groups

    def get_admin_articles(self):
        return self.article_set.all()


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    email_confirmed = models.BooleanField(default=False)
    bio = RichTextField(max_length=500, blank=True, verbose_name='About me')
    location = models.CharField(max_length=30, blank=True)
    birth_date = models.DateField(null=True, blank=True)
    job_description = models.CharField(max_length=100, null=True, blank=True)
    hobbies = models.CharField(max_length=100, null=True, blank=True)

    @classmethod
    def create(cls, user):
        profile = cls()
        profile.user = user
        profile.save()


class PostedObject(models.Model):
    """ A model to contain values which are required for every type of posted text. Can be extended with a body if
    more text is required"""
    title = models.CharField(max_length=100, blank=True)
    date = models.DateTimeField(auto_now_add=True, db_index=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        abstract = True


class Article(PostedObject):
    body = RichTextField(max_length=50000, blank=True, verbose_name='Article text')
    private = models.BooleanField(default=False, verbose_name='Only show to members of the chosen group?')


class Comment(PostedObject):
    body = models.CharField(max_length=1000, blank=True)
    article = models.ForeignKey(Article, on_delete=models.CASCADE)


class PublishGroup(models.Model):
    name = models.CharField(max_length=100, blank=True)
    description = models.TextField(null=True, blank=True, verbose_name="Group description (optional)")
    made_by = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)
    created = models.DateTimeField(verbose_name=_("Created"),
                                   auto_now_add=True, db_index=True)
    modified = models.DateTimeField(verbose_name=_("Modified"),
                                    auto_now=True, db_index=True)

    @property
    def group_size(self):
        return len(self.membership_set.filter(invite_status='APPROVED'))

    def get_is_admin(self, user):
        try:
            membership = self.membership_set.get(user=user)
            is_admin = membership.is_admin
        except Membership.DoesNotExist:
            is_admin = None
        return is_admin

    def get_admins(self):
        return self.membership_set.filter(is_admin=True)


class Membership(models.Model):
    """Intermediary model to handle logic about User being members of Groups. Used to decouple the two models."""
    STATUS_CHOICES = (('INVITED', 'Invited'),
                      ('APPROVED', 'Approved'),
                      ('REJECTED', 'Rejected'))

    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    group = models.ForeignKey(PublishGroup, on_delete=models.CASCADE)
    is_admin = models.BooleanField(default=False)
    date_joined = models.DateTimeField(auto_now_add=True)
    invite_email = models.EmailField()
    invite_status = models.CharField(max_length=20, choices=STATUS_CHOICES, default='INVITED')
    invite_sent = models.DateTimeField(auto_now_add=True)
    invite_accepted = models.DateTimeField(null=True, blank=True, default=None)

    @classmethod
    def create(cls, group_id, email):
        membership = cls()
        membership.group = PublishGroup.objects.get(id=group_id)
        membership.invite_email = email
        membership.save()
        return membership

    @classmethod
    def create_without_email(cls, group, user, email, admin=False):
        membership = cls()
        membership.group = group
        membership.user = user
        membership.invite_status = 'APPROVED'
        membership.invite_email = email
        membership.invite_accepted = datetime.now()
        membership.is_admin = admin

        membership.save()
        return membership

    def send_invite(self, sender, body):
        if len(User.objects.filter(email=self.invite_email)) > 0:
            user_name = User.objects.get(email=self.invite_email).username
        else:
            user_name = self.invite_email
        context = {'user_name': user_name,
                   'group_name': self.group.name,
                   'admin_name': sender,
                   'body': body}
        msg_html = render_to_string('cms/emails/invite.html', context)
        email = EmailMessage(
            subject='An invitation to a Serendipity group!',
            from_email='noreply@serendipity.com',
            to=[self.invite_email],
            body=msg_html,
        )
        email.content_subtype = "html"  # Main content is now text/html
        email.send()


class GroupPost(models.Model):
    """Intermediary model to keep track of which Groups an Article has been posted to. Decouples the models and removes
    the need for a ManyToManyField"""
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    group = models.ForeignKey(PublishGroup, on_delete=models.CASCADE)
