from django.urls import include, path
from django_cms.cms import views

urlpatterns = [
    path('', views.home_page, name='home'),
    path('user/<int:pk>/my_profile', views.ProfileUpdateView.as_view(), name='profile'),
    path('user/<int:pk>/profile', views.ProfileDetailView.as_view(), name='user_profile'),
    path('groups', views.PublishGroupListView.as_view(), name='groups'),
    path('users', views.UserListView.as_view(), name='users'),
    path('ajax/toggle_admin/<int:pk>/', views.toggle_user_admin, name='toggle_admin'),
    path('all_groups', views.GroupListView.as_view(), name='all_groups'),
    path('my_groups', views.MembershipListView.as_view(), name='my_groups'),
    path('new_group', views.GroupCreateView.as_view(), name='new_group'),
    path('group/<int:pk>', views.GroupDetailView.as_view(), name='group'),
    path('group/<int:pk>/users', views.GroupMembershipListView.as_view(), name='group_users'),
    path('group/<int:pk>/edit', views.GroupUpdateView.as_view(), name='group_edit'),
    path('group/<int:pk>/delete', views.GroupDeleteView.as_view(), name='group_delete'),
    path('membership/<int:pk>/accept', views.accept_membership, name='membership_accept'),
    path('membership/<int:pk>/reject', views.reject_membership, name='membership_reject'),
    path('join_group/<int:pk>', views.join_group, name='join_group'),
    path('articles', views.ArticleListView.as_view(), name='articles'),
    path('new_article', views.ArticleCreateView.as_view(), name='new_article'),
    path('article/<int:pk>', views.ArticleDetailView.as_view(), name='article'),
    path('article/<int:pk>/edit', views.ArticleUpdateView.as_view(), name='article_edit'),
    path('article/<int:pk>/delete', views.ArticleDeleteView.as_view(), name='article_delete'),
]
