from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.contrib.auth import login, get_user_model
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django_cms.cms.tokens import account_activation_token
from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
from django_cms.cms.forms import SignUpForm
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView
from django.views.generic import DetailView
from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from datetime import datetime

import django_cms.cms.models as models
import django_cms.cms.forms as forms


def home_page(request):
    return render(request, 'cms/site/home.html')


def account_activation_sent(request):
    return render(request, 'registration/email_sent.html')


def activate(request, uidb64, token):
    User = get_user_model()
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None

    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.profile.email_confirmed = True
        user.save()
        login(request, user)
        return redirect('home')
    else:
        return render(request, 'registration/account_activation_invalid.html')


def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.is_active = False
            user.save()
            current_site = get_current_site(request)
            subject = 'Activate Your CMS Account'
            message = render_to_string('registration/account_activation_email.html', {
                'user': user,
                'domain': current_site.domain,  # 127.0.0.1:8000
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'token': account_activation_token.make_token(user),
            })
            user.email_user(subject, message)
            return redirect('account_activation_sent')
    else:
        form = SignUpForm()
    return render(request, 'registration/signup.html', {'form': form})


def password_reset(request):
    return render(request, 'registration/password_reset_form.html')


def toggle_user_admin(request, pk):
    user = models.User.objects.get(id=pk)
    user.is_superuser = not user.is_superuser
    user.is_staff = user.is_superuser
    user.save()
    return JsonResponse({'admin_status': user.is_superuser})


@method_decorator(login_required, name='dispatch')
class ProfileUpdateView(UpdateView):
    model = models.Profile
    form_class = forms.ProfileForm
    template_name = 'cms/site/edit_profile.html'

    def get_queryset(self):
        queryset = super(ProfileUpdateView, self).get_queryset()
        queryset = queryset.filter(user=self.request.user)
        return queryset

    def get_success_url(self):
        return reverse_lazy('profile', kwargs={'pk': self.object.user.pk})


@method_decorator(login_required, name='dispatch')
class ProfileDetailView(DetailView):
    context_object_name = 'profile'
    template_name = 'cms/site/detail_profile.html'
    model = models.Profile

    def get_context_data(self, **kwargs):
        context = super(ProfileDetailView, self).get_context_data(**kwargs)
        context['back_url'] = self.request.GET.get('back', '/default/url/')
        return context


@method_decorator(login_required, name='dispatch')
class PublishGroupListView(ListView):
    model = models.PublishGroup


@method_decorator(login_required, name='dispatch')
class UserListView(ListView):
    context_object_name = 'users'
    model = models.User
    template_name = 'cms/site/user_list.html'
    ordering = 'username'


@method_decorator(login_required, name='dispatch')
class GroupListView(ListView):
    context_object_name = 'groups'
    template_name = 'cms/site/all_groups.html'
    model = models.PublishGroup
    ordering = '-id'

    def get_queryset(self):
        base_queryset = super(GroupListView, self).get_queryset()
        for group in base_queryset:
            group.is_admin = group.get_is_admin(self.request.user)
            group.joined = len(models.Membership.objects.filter(user=self.request.user, group__id=group.id)) > 0
        return base_queryset


@method_decorator(login_required, name='dispatch')
class MembershipListView(ListView):
    context_object_name = 'groups'
    template_name = 'cms/site/my_groups.html'
    model = models.Membership
    ordering = '-id'

    def get_queryset(self):
        base_queryset = super(MembershipListView, self).get_queryset()
        user_queries = base_queryset.filter(user=self.request.user)
        queryset = {'active': user_queries.filter(invite_status='APPROVED', is_admin=False),
                    'invites': base_queryset.filter(invite_email=self.request.user.email, invite_status='INVITED'),
                    'admin': self.request.user.get_admin_groups()}
        for membership in queryset['active']:
            membership.is_admin = membership.group.get_is_admin(self.request.user)
            membership.group_size = len(membership.group.membership_set.filter(invite_status='APPROVED'))
        return queryset


@method_decorator(login_required, name='dispatch')
class GroupMembershipListView(ListView):
    context_object_name = 'members'
    template_name = 'cms/site/group_members.html'
    model = models.Membership
    ordering = 'user__last_name'

    def get_queryset(self):
        base_queryset = super(GroupMembershipListView, self).get_queryset()
        group_queries = base_queryset.filter(group=self.kwargs['pk'])
        queryset = {'active': group_queries.filter(invite_status='APPROVED'),
                    'invites': group_queries.filter(invite_status='INVITED'),
                    'rejected': group_queries.filter(invite_status='REJECTED')}
        return queryset

    def get_context_data(self, **kwargs):
        context = super(GroupMembershipListView, self).get_context_data(**kwargs)
        context['group'] = models.PublishGroup.objects.get(id=self.kwargs['pk'])
        context['is_admin'] = context['group'].get_is_admin(self.request.user)
        context['form'] = forms.AddUsersForm()
        return context

    def post(self, request, *args, **kwargs):
        emails = forms.AddUsersForm.clean_emails(request.POST['emails'])
        for email in emails:
            if len(models.Membership.objects.filter(group__id=self.kwargs['pk'], user__email=email)) == 0:
                membership = models.Membership.create(group_id=self.kwargs['pk'], email=email)
                membership.send_invite(sender=str(self.request.user), body=request.POST['body'])
            else:
                print('User already exists.')
        return redirect(reverse_lazy('group_users', kwargs={'pk': self.kwargs['pk']}))


@method_decorator(login_required, name='dispatch')
class GroupCreateView(CreateView):
    model = models.PublishGroup
    form_class = forms.GroupForm
    success_url = reverse_lazy('my_groups')
    template_name = 'cms/site/new_group.html'

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.made_by = self.request.user
        obj.save()
        return super().form_valid(form)


@method_decorator(login_required, name='dispatch')
class GroupDetailView(DetailView):
    context_object_name = 'group'
    template_name = 'cms/site/detail_group.html'
    model = models.PublishGroup

    def get_context_data(self, **kwargs):
        context = super(GroupDetailView, self).get_context_data(**kwargs)
        context['is_admin'] = self.object.get_is_admin(self.request.user)
        context['admin_string'] = ", ".join([str(x.user.get_full_name()) for x in self.object.get_admins()])
        context['user_count'] = len(self.object.membership_set.filter(invite_status='APPROVED'))
        return context


@method_decorator(login_required, name='dispatch')
class GroupDeleteView(DeleteView):
    model = models.PublishGroup
    context_object_name = 'group'
    template_name = 'cms/site/delete_group.html'
    success_url = reverse_lazy('my_groups')

    def get_queryset(self):
        return self.request.user.get_admin_groups()


@method_decorator(login_required, name='dispatch')
class GroupUpdateView(UpdateView):
    context_object_name = 'group'
    model = models.PublishGroup
    form_class = forms.GroupForm
    template_name = 'cms/site/edit_group.html'

    def get_queryset(self):
        return self.request.user.get_admin_groups()

    def get_success_url(self):
        return reverse_lazy('group', kwargs={'pk': self.object.pk})


@login_required
def accept_membership(request, pk):
    membership = models.Membership.objects.get(id=pk)
    membership.invite_status = 'APPROVED'
    membership.invite_accepted = datetime.now()
    membership.user = request.user
    membership.save()
    return redirect(reverse_lazy('group', kwargs={'pk': membership.group.pk}))


@login_required
def reject_membership(request, pk):
    membership = models.Membership.objects.get(id=pk)
    membership.invite_status = 'REJECTED'
    membership.invite_accepted = datetime.now()
    membership.save()
    return redirect(reverse_lazy('my_groups', kwargs={'pk': request.user.pk}))


@login_required
def join_group(request, pk):
    membership = models.Membership.create_without_email(group=models.PublishGroup.objects.get(id=pk),
                                                        user=request.user,
                                                        email=request.user.email)
    return redirect(reverse_lazy('group', kwargs={'pk': membership.group.pk}))


@method_decorator(login_required, name='dispatch')
class ArticleListView(ListView):
    context_object_name = 'articles'
    template_name = 'cms/site/articles.html'
    model = models.Article
    ordering = '-id'

    def get_queryset(self):
        base_queryset = super(ArticleListView, self).get_queryset()
        queryset = {'mine': base_queryset.filter(user=self.request.user),
                    'all': base_queryset.exclude(private=True, )}
        return queryset


@method_decorator(login_required, name='dispatch')
class ArticleCreateView(CreateView):
    model = models.Article
    form_class = forms.ArticleForm
    template_name = 'cms/site/new_article.html'

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.user = self.request.user
        obj.save()
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy('article', kwargs={'pk': self.object.pk})


@method_decorator(login_required, name='dispatch')
class ArticleDetailView(DetailView):
    context_object_name = 'article'
    template_name = 'cms/site/detail_article.html'
    model = models.Article

    def get_context_data(self, **kwargs):
        context = super(ArticleDetailView, self).get_context_data(**kwargs)
        context['is_admin'] = (self.object.user == self.request.user)
        return context


@method_decorator(login_required, name='dispatch')
class ArticleUpdateView(UpdateView):
    context_object_name = 'article'
    model = models.Article
    form_class = forms.ArticleForm
    template_name = 'cms/site/edit_article.html'

    def get_queryset(self):
        return self.request.user.get_admin_articles()

    def get_success_url(self):
        return reverse_lazy('article', kwargs={'pk': self.object.pk})


@method_decorator(login_required, name='dispatch')
class ArticleDeleteView(DeleteView):
    model = models.Article
    template_name = 'cms/site/delete_article.html'
    context_object_name = 'article'
    success_url = reverse_lazy('articles')

    def get_queryset(self):
        return self.request.user.get_admin_articles()
