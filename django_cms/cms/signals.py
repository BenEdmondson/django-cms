from django.db.models.signals import post_save
from django.dispatch import receiver
import django_cms.cms.models as app_models


@receiver(post_save, sender=app_models.User)
def update_user_profile(sender, instance, created, **kwargs):
    if created:
        app_models.Profile.create(user=instance)


@receiver(post_save, sender=app_models.PublishGroup)
def create_group_admin_membership(sender, instance, created, **kwargs):
    if created:
        membership = app_models.Membership.create_without_email(group=instance, user=instance.made_by, email=instance.made_by.email, admin=True)
        membership.save()
