from django.apps import AppConfig


class SignalConfig(AppConfig):
    name = 'django_cms.cms'

    def ready(self):
        import django_cms.cms.signals
