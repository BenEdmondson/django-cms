from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import get_user_model
import django_cms.cms.models as models
from django.core.validators import validate_email
from django.core.exceptions import ValidationError


class SignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
    last_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
    email = forms.EmailField(max_length=254, help_text='Required. Please use a valid email address.')

    class Meta:
        model = get_user_model()
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2', )


class ProfileForm(forms.ModelForm):
    birth_date = forms.DateField(widget=forms.DateInput(attrs={'type': 'date', 'class': 'form-control'}),
                                 label='Date of Birth', input_formats=['%d-%m-%Y', '%Y-%m-%d'])

    class Meta:
        model = models.Profile
        fields = ['bio', 'location', 'birth_date', 'job_description', 'hobbies']


class AddUsersForm(forms.Form):
    emails = forms.CharField(widget=forms.Textarea(attrs={'rows': 1, 'cols': 116}),
                             help_text='Multiple emails can be separated by commas')
    body = forms.CharField(widget=forms.Textarea(attrs={'cols': 116}), label='Custom Email text',
                           initial='Would you like to join our Article Group?')

    @staticmethod
    def clean_emails(emails):
        email_list = emails.split(',')
        valid_emails = []
        for email in email_list:
            try:
                email = email.replace(" ", "")
                validate_email(email)
                valid_emails.append(email)
            except ValidationError:
                pass
        return valid_emails


class GroupForm(forms.ModelForm):
    class Meta:
        model = models.PublishGroup
        fields = ['name', 'description']


class ArticleForm(forms.ModelForm):
    class Meta:
        model = models.Article
        fields = ['title', 'body', 'private']
